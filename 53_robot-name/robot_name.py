from string import ascii_uppercase, digits
from secrets import choice


class Robot(object):

    already_used_names = list()

    def __init__(self):

        self.__rename()

    def reset(self):

        oldname = self.name

        self.__rename()

        ####  make the old name available,
        # (comment the next line if you don't want)
        Robot.already_used_names.remove(oldname)

    def __rename(self):

        while True:
            self.name = choice(ascii_uppercase) \
                + choice(ascii_uppercase) \
                + choice(digits) \
                + choice(digits) \
                + choice(digits)

            if self.name not in Robot.already_used_names:

                break

        Robot.already_used_names.append(self.name)

