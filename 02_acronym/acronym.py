def abbreviate(words=''):

    return ''.join([word[0] \
        for word in words.upper().replace("-", " ").replace("_", " ").split() \
    ])