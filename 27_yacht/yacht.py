YACHT  = lambda d: 50 if len(set(d)) == 1 else 0
ONES   = lambda d: d.count(1)
TWOS   = lambda d: d.count(2) * 2 
THREES = lambda d: d.count(3) * 3
FOURS  = lambda d: d.count(4) * 4
FIVES  = lambda d: d.count(5) * 5
SIXES  = lambda d: d.count(6) * 6
FULL_HOUSE      = lambda d: sum(d) if len(set(d)) == 2 and d.count(d[0]) in [2, 3] else 0
FOUR_OF_A_KIND  = lambda d: sum(4 * i for i in set(d) if d.count(i) >= 4)
LITTLE_STRAIGHT = lambda d: 30 * (sorted(d) == [1, 2, 3, 4, 5])
BIG_STRAIGHT    = lambda d: 30 * (sorted(d) == [2, 3, 4, 5, 6])
CHOICE = lambda d: sum(d)

def score(dice, category):
    assert(type(dice) == list)
    assert(len(dice) == 5)
    assert(set(dice).issubset(range(1,7)))
    assert(category in [YACHT, ONES, TWOS, THREES, FOURS, FIVES, SIXES, FULL_HOUSE, FOUR_OF_A_KIND, LITTLE_STRAIGHT, BIG_STRAIGHT, CHOICE])
    
    return category(dice)