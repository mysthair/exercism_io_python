def square_of_sum(n):
    return ((n * (n + 1)) // 2) ** 2


def sum_of_squares(n):
    return (n * (n + 1) * (2 * n + 1)) // 6


def difference(n):
    return (n * (n - 1) * (n + 1) * (3 * n + 2) ) // 12
