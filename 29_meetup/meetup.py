from datetime import date

class MeetupDayException(IndexError):
    def __init__(self, *args):
        IndexError.__init__(self, *args)
    

def meetup_day(year, month, day_of_the_week, which):

    assert(type(year) == int)
    assert(month in list(range(1,13)))
    assert(day_of_the_week in ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'])
    assert(which in ['1st', '2nd', '3rd', '4th', '5th', 'teenth', 'last'])

    index = { '1st': 0, '2nd': 1, '3rd': 2, '4th': 3, '5th': 4, 'last':-1 }

    weekday = dict( (date(2001, 1, i).strftime("%A"), date(2001, 1, i).weekday()) for i in range(1,8) )[day_of_the_week]

    if which in index.keys():
        array = []
        for day in range(1,32):
            try:
                if date(year, month, day).weekday() == weekday:
                    array.append(date(year, month, day))
            except ValueError:
                pass
        if index[which] < len(array): #non existant meetup_day(2015, 2, 'Monday', '5th')
            return array[index[which]]
        raise MeetupDayException("Invalid value")

    elif which == 'teenth':
        for day in range(13,20):
            if date(year, month, day).weekday() == weekday:
                return date(year, month, day)

    raise MeetupDayException("Bad which identifier")
