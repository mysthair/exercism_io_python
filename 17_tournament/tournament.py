FORMAT='{:<30} |{:>3} |{:>3} |{:>3} |{:>3} |{:>3}'

def tally(tournament_results):
    scores = {}
    reverse={'win':'loss', 'draw':'draw', 'loss':'win' }

    def update_team(team, res):
        score = scores.get(team, {'Team':team, 'MP':0, 'W':0, 'D':0, 'L':0, 'P':0})
        score['MP'] += 1
        score['W'] += (res == 'win')
        score['D'] += (res == 'draw')
        score['L'] += (res == 'loss')
        score['P'] += 3 * (res == 'win') + (res == 'draw') 
        scores[team] = score
    
    for match in tournament_results.splitlines():
        teamA, teamB, res = match.split(';')
        update_team(teamA, res)
        update_team(teamB, reverse[res])

    return '\n'.join( [ FORMAT.format('Team', 'MP', 'W', 'D', 'L', 'P') ]
        + [ FORMAT.format(t['Team'],t['MP'],t['W'],t['D'],t['L'], t['P'])
            for t in sorted(sorted(scores.values(), key=lambda s:s['Team']), key=lambda r:r['P'], reverse=True) 
    ])
