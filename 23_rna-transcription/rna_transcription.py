TRANSCRIPTION = {'G':'C', 'C':'G', 'T':'A', 'A':'U' }


def to_rna(dna_strand):
    return ''.join( TRANSCRIPTION[c] for c in dna_strand )
