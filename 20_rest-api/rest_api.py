import json

class RestAPI(object):

    class User:
        def __init__(self, name, data=None):
            self.name = name
            self.owes = data['owes'] if data!=None else dict()
            self.owed_by = data['owed_by'] if data!=None else dict()
            self.balance = data['balance'] if data!=None else 0

        def to_json(self):
            return {
                'name': self.name,
                'owes': self.owes,
                'owed_by': self.owed_by,
                'balance': self.balance
            }

        def iou(self, other, amount):
            assert(self.owed_by.get(other.name, 0) == other.owes.get(self.name, 0))
            assert(self.owes.get(other.name, 0) == other.owed_by.get(self.name, 0))
            assert(self.owed_by.get(other.name, 0) - self.owes.get(other.name, 0)
                 == other.owes.get(self.name, 0) - other.owed_by.get(self.name, 0))

            self.owed_by[other.name] = amount + self.owed_by.get(other.name, {other.name : 0})[other.name]
            self.balance += amount
            
            other.owes[self.name] = amount + self.owes.get(self.name, {self.name : 0})[self.name]
            other.balance -= amount

            balance = self.owed_by.get(other.name, 0) - self.owes.get(other.name, 0)

            if balance >= 0:
                if balance != self.owed_by.get(other.name, 0):

                    if balance > 0:
                        self.owed_by[other.name] = balance
                        other.owes[self.name] = balance
                    else:
                        if other.name in self.owed_by: del self.owed_by[other.name]
                        if self.name in other.owes: del other.owes[self.name]

                    if other.name in self.owes: del self.owes[other.name]
                    if self.name in other.owed_by: del other.owed_by[self.name]

            else:
                if -balance != self.owes.get(other.name, 0):

                    self.owes[other.name] = - balance
                    other.owed_by[self.name] = - balance

                    if other.name in self.owed_by: del self.owed_by[other.name]
                    if self.name in other.owes: del other.owes[self.name]

    class Users:
        def __init__(self, database):
            self.users = dict()
            if database:
                for user in database['users']:
                    self.users[user['name']] = RestAPI.User(user['name'], user)

        def get(self, names=None):
            if names == None:
                return self.users.values()
            return [user for user in self.users.values() if user.name in names]

        def add(self, name):
            user = self.users.get(name, None)
            if user == None:
                user = self.users[name] = RestAPI.User(name)
            return user

        def to_json(self):
            return [user.to_json() for user in self.users.values()]

    def __init__(self, database=None):
        self.users = RestAPI.Users(database)
    
    def get(self, url, payload=None):
        if url == '/users':
            if payload == None:
                return json.dumps({ 'users': self.users.to_json() })
            else:
                payload = json.loads(payload)
                return json.dumps({ 'users' : [user.to_json() for user in self.users.get(payload['users'])]})

    def post(self, url, payload=None):

        if url == '/add':
            payload = json.loads(payload)
            assert(payload != None)
            assert(len(str(payload['user'])) > 0)
            
            return json.dumps(self.users.add(payload['user']).to_json())
        
        elif url == '/iou':
            
            payload = json.loads(payload)

            borrower = self.users.add(payload['borrower']) 
            lender = self.users.add(payload['lender'])
        
            lender.iou(borrower, payload['amount'])

            return json.dumps({'users': [ user.to_json() 
                for user in sorted([lender, borrower], key=lambda u:u.name) ]})
        else:
            raise Exception("404")
