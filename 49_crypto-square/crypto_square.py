import re

def encode(plain_text):
    normalized = ''.join(c for c in plain_text.lower() if c.isalnum())
    
    nbcolumns = 1 
    while nbcolumns * nbcolumns < len(normalized):
        nbcolumns += 1
    
    rectangle = re.findall(r'.{1,' + str(nbcolumns) + '}', normalized)

    if len(rectangle) > 0 and len(rectangle[-1]) < nbcolumns:
        rectangle[-1] += ' ' *  (nbcolumns - len(rectangle[-1]))
    
    return ' '.join( ''.join(line) for line in zip(*rectangle))