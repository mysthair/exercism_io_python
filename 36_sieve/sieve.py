def sieve(limit):
    if limit < 2:
        return []
    
    primes = [2]
    work = list(range(3, limit+1, 2))

    while len(work):
        prime = work.pop(0)
        primes.append(prime)
    
        m = prime + prime + prime
        while m < limit + 1:
            if m in work:
                work.remove(m)
            m += prime + prime
    
    return primes
