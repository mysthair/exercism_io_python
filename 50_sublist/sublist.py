EQUAL       = lambda K, L: K == L
SUBLIST = lambda K, L: len(K) < len(L) \
    and sum(K == L[idx: idx + len(K)] for idx in range(0, len(L) + 1 - len(K)))
SUPERLIST   = lambda K, L: SUBLIST(L, K)
UNEQUAL     = lambda K, L: not (EQUAL(K,L) or SUBLIST(K,L) or SUBLIST(L,K))

def check_lists(first_list, second_list):
    for op in [EQUAL, SUBLIST, SUPERLIST]:
        if op(first_list, second_list):
            return op
    return UNEQUAL