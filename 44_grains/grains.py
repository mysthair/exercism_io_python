def check_arg_in_1_64(func):
    def _wrapper(*args,**kwargs):
        if args[0] <= 0 or args[0] > 64:
            raise ValueError('Invalid value')
        return func(*args,**kwargs)
    return _wrapper
    
        
@check_arg_in_1_64
def on_square(integer_number):
    return 1 << (integer_number - 1)

@check_arg_in_1_64
def total_after(integer_number):
    return (1 << integer_number) -1
