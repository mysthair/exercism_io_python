class BufferFullException(Exception):
    pass


class BufferEmptyException(Exception):
    pass


class CircularBuffer(object):
    def __init__(self, capacity):
        assert(capacity > 0)
        self._buffer    = [ '#' for i in range(0, capacity) ]
        self._capacity  = capacity
        self._start     = 0
        self._unreaded    = 0

    def read(self):
        if self._unreaded == 0:
            raise BufferEmptyException('Empty buffer')
        
        c = self._buffer[self._start]
        
        self._start = (self._start + 1) % self._capacity
        
        self._unreaded -= 1

        return c

    def write(self, data):
        assert(len(data) == 1)

        if self._capacity - self._unreaded == 0:
            raise BufferFullException('Buffer is full')
        
        self._buffer[ (self._start + self._unreaded) % self._capacity ] = data[0]

        self._unreaded += 1

    def overwrite(self, data):
        assert(len(data) == 1)
        
        self._buffer[ (self._start + self._unreaded) % self._capacity ] = data[0]

        if self._unreaded < self._capacity:
            self._unreaded += 1
        else:
            self._start = (self._start + 1) % self._capacity

    def clear(self):

        self._unreaded = 0
