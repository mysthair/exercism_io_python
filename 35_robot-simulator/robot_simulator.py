# Globals for the bearings
# Change the values as you see fit
EAST  = ( 1,  0)
NORTH = ( 0,  1)
WEST  = (-1,  0)
SOUTH = ( 0, -1)

DIRECTIONS=[NORTH, EAST, SOUTH, WEST]

class Robot(object):
    def __init__(self, bearing=NORTH, x=0, y=0):
        self._x = x
        self._y = y
        self.bearing = bearing

    def advance(self):
        self._x += self.bearing[0]
        self._y += self.bearing[1]

    @property
    def coordinates(self):
        return (self._x, self._y)


    def turn_left(self):
        self.bearing = DIRECTIONS[(3 + DIRECTIONS.index(self.bearing)) % 4]
    def turn_right(self):
        self.bearing = DIRECTIONS[(1 + DIRECTIONS.index(self.bearing)) % 4]
        

    def simulate(self, code):
        for a in code:
            if a == 'A':
                self.advance()
            elif a == 'L':
                self.turn_left()
            elif a == 'R':
                self.turn_right()
            else:
                raise Exception("Bad instruction.")