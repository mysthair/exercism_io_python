class Matrix(object):
    def __init__(self, matrix_string):
    
        self._row = [ [ int(i) for i in line.split() ] for line in matrix_string.splitlines() ]
        
        self._column = [ list(column) for column in list(zip(*self._row))]

    def row(self, index):

        return self._row[ index - 1 ]

    def column(self, index):
        
        return self._column[ index - 1 ]
