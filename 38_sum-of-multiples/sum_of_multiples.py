def sum_of_multiples(limit, multiples):
    if 0 in multiples:
        multiples.remove(0)
    return sum( set(d for d in range(1, limit) for p in multiples if d % p == 0 ))
