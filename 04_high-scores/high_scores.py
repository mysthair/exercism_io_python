class HighScores(object):

  def __init__(self, scores):
    self.scores = scores[:]

  def latest(self):
    return self.scores[-1]

  def report(self):
    best = self.personal_best()
    latest = self.latest()
    diff = best - latest

    if diff == 0:
      return f"Your latest score was { best }. That's your personal best!"

    return f"Your latest score was { latest }. That's { diff } short of your personal best!"

  def personal_best(self):
    return max(self.scores) 

  def personal_top(self):
    return sorted(self.scores, reverse=True)[:3]
