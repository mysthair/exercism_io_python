class Luhn(object):

    _MIX = [list(range(0,10)), [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]]

    def __init__(self, card_num):
        self.card_num = card_num.replace(" ", "")

    def is_valid(self):
        return len(self.card_num) > 1 \
        and set(self.card_num).issubset("0123456789 ") \
        and 0 == sum(Luhn._MIX[i % 2][int(c)] \
            for i, c in enumerate(reversed(self.card_num))) % 10