class SpaceAge(object):
    def __init__(self, seconds):
        self.seconds = seconds
    
    def on_earth(self):
        return round(self.seconds / 31557600, 2) #* 31.69 / 1000000000

    def on_jupiter(self):
        return round(self.seconds * 2.41 / 901876382, 2)
    
    def on_mars(self):
        return round(self.seconds * 39.25 / 2329871239, 2)

    def on_mercury(self):
        return round(self.seconds * 280.88 / 2134835688, 2)
        
    def on_neptune(self):
        return round(self.seconds * 1.58 / 8210123456, 2)

    def on_saturn(self):
        return round(self.seconds * 3.23 / 3000000000, 2)
    
    def on_uranus(self):
        return round(self.seconds * 1.21 / 3210123456, 2)

    def on_venus(self):
        return round(self.seconds * 9.78 / 189839836, 2)
        