UNITARY_COST = 8
DISCOUNT=[0, 0, 5, 10, 20, 25]
PRICES = [n * UNITARY_COST * (100 - DISCOUNT[n]) for n in range(0, 6)]


def calculate_total(books):

    data_0 = books[:]
    work_0 = []
    mini = UNITARY_COST * len(data_0) * 100

    def recurs(mini, data, work):
        if len(data) == 0:
            return min(mini, price(work)) 
        x = data.pop(0)
        for i in range(0,len(work)):
            e = work[i]
            if x not in e and e not in work[0:i]:
                e.add(x)
                mini = min(mini, recurs(mini, data, work))
                e.remove(x)

        work.append({ x })
        mini = min(mini, recurs(mini, data, work))
        work.pop()
        data.insert(0, x)
        return mini
        
    def price(work):
        prix = sum ( PRICES[len(e)] for e  in work )
        #print(f"{prix} : {work}")
        return prix

    if len(books) == 0:
        return 0

    return recurs(mini, data_0, work_0)



if __name__=='__main__':
    def show(l,n):
        print(f"calcul de calculate_total({l}) en cours ...") 
        r = calculate_total(l)
        print(f"calculate_total({l}) = {r}")
        assert(r == n)
    if True:
        show([1], 800)

        show([2, 2], 1600)

        show([], 0)

        show([1, 2], 1520)

        show([1, 2, 3], 2160)

        show([1, 2, 3, 4], 2560)

        show([1, 2, 3, 4, 5], 3000)

        show([1, 1, 2, 2, 3, 3, 4, 5], 5120)

        show([1, 1, 2, 2, 3, 4], 4080)

        show([1, 1, 2, 2, 3, 3, 4, 4, 5], 5560)
        
        show([1, 1, 2, 2, 3, 3, 4, 4, 5, 5], 6000)

        show([1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 1], 6800)

        show([1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 1, 2], 7520)
    
        print("(L'ensemble des calculs précedents prend 0,52s sur mon portable.)")

    else:
    
        print("(Rq: Ce calcul prends 146s ie 2 minutes et 26 secondes sur mon portable)")

        show([1, 1, 2, 2, 3, 3, 4, 5, 1, 1, 2, 2, 3, 3, 4, 5], 10240)
