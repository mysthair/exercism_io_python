def triplets_with_sum(sum_of_triplet):
    if sum_of_triplet & 1:
        return set()
    triplets = []
    for r in range(2, sum_of_triplet//3, 2): 
        S = (sum_of_triplet - 3*r)//2
        P = r*r // 2
        delta = S*S - 4*P
        if delta >= 0:
            sqd = int(delta ** 0.5)
            while sqd * sqd < delta:
                sqd += 1
            if sqd * sqd == delta:
                if (S + sqd) & 1 == 0:
                    s , t = (S - sqd)//2, (S + sqd)//2
                    x, y, z = r+s, r+t, r+s+t
                    triplets.append( (x, y, z) )
    return set(triplets)

SomeExplanation="""
def triplets_with_sum(sum_of_triplet):
    # Dickson's method : https://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples#Dickson's_method
    # trinomial method : https://en.wikipedia.org/wiki/Quadratic_equation

    # if we choose r,s,t such that r*r = 2*s*t
    # then let x=r+s, y=r+t, z = r+s+t verify x*x + y*y = z*z 

    # let's proof it :
    # x*x + y*y = (r+s)*(r+s) + (r+t)*(r+t)
    #           = r*r + 2*r*s + s*s + r*r + 2*r*t + t*t
    #           = r*r + 2*r*s + s*s + 2*s*t + 2*r*t + t*t  # because r*r = 2*s*t
    #           = r*r + s*s + t*t + 2*r*s + 2*s*t + 2*r*t
    #           = (r+s+t)*(r+s+t)
    #           = z*z                

    # note 1:
    #   sum_of_triplet = x+y+z
    #                  = r+s + r+t + r+s+t
    #                  = 3*r + 2*(s+t)
    # ie  s+t = (sum_of_triplet - 3*r)/2    who must be natural
    # or  s*t = r*r / 2                     who must be natural too
    # 
    # ie s and t are solution of X*X - S*X + P = 0  where S = s+t and P = s*t
    #    S = (sum_of_triplet - 3*r)/2
    #    P = r*r / 2
    #    delta = S*S - 4*P 
    # solutions:   s = (S - sqrt(delta))/2 
    #              t = (S + sqrt(delta))/2   
    #            (if s and t be natural ... of course)

    # note 2:
    # P = r*r / 2 must be integer  
    # =>  r must be even
    # S = (sum_of_triplet - 3*r)/2 must be integer 
    # =>  sum_of_triplet must be even too 
    if sum_of_triplet & 1:
        return set()

    triplets = []
    # note 3:
    #    r = (sum_of_triplet - 2*(s+t))/3   
    # => r < sum_of_triplet/3
    for r in range(2, sum_of_triplet//3, 2): # and r must be even 
        S = (sum_of_triplet - 3*r)//2
        P = r*r // 2
        delta = S*S - 4*P
        #note 4:
        # delta must be positive, or else solutions are complex
        if delta >= 0:
            sqd = int(delta ** 0.5)
            while sqd * sqd < delta:
                sqd += 1
            if sqd * sqd == delta:
                if (S + sqd) & 1 == 0:
                    s , t = (S - sqd)//2, (S + sqd)//2
                    x, y, z = r+s, r+t, r+s+t
                    triplets.append( (x, y, z) )
    return set(triplets)
"""
