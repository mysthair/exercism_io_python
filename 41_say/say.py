def say(number):
    number = int(number)
    mydict = {0:'zero', 1:'one', 2: 'two', 3:'three', 4:'four', 5:'five', 6:'six', 7:'seven', 8:'eight', 9:'nine' }
    
    dizaine = { 10:'ten', 11:'eleven', 12: 'twelve', 13:'thirteen', 14: 'fourteen', 15:'fifteen', 16:'sixteen', 17:'seventeen', 18:'eighteen', 19: 'nineteen' }
    dizaines = { 20: 'twenty', 30:'thirty', 40:'forty', 50:'fifty', 60:'sixty', 70:'seventy', 80: 'eighty', 90: 'ninety' }
    
    ions = { 10**9:'billion', 10**6: 'million', 1000:'thousand', 100:'hundred' }

    if number < 0 or number > (10**12)-1:
        raise ValueError("negative number")

    if number in mydict.keys():
        return mydict[number]

    result=[]

    for ion in ions.keys():
        if number // ion:
            result.append(say(number // ion))
            result.append(ions[ion])
            number %= ion

    if len(result) > 0 and number > 0:
        result.append('and')

    if number -(number % 10) in  dizaines.keys():
        result.append(dizaines[number -(number % 10)])
        number %= 10
        if number > 0:
            result[-1] += '-' + say(number % 10)
    elif number -(number % 10) == 10:
        result.append(dizaine[number])
    elif number > 0:
        result.append(mydict[number])

    return ' '.join(result)
