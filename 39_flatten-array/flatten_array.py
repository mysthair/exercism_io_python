def flatten(iterable):
    
    result = []
    
    for c in iterable:
        if isinstance(c, (list, tuple)):
            result += flatten(c)
        elif c != None:
            result.append(c)
    
    return result
