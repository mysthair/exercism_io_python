import re


def parse_markdown(markdown):
    code = []
    block_end = ''

    for line in markdown.splitlines():
        
        if line.startswith('#'):
            
            match = re.match(r'(#+) (.*)', line)
            line = '<h{0}>{1}</h{0}>'.format(len(match.group(1)), match.group(2)) 
            
            if len(block_end) > 0:
                code.append(block_end)
                block_end = ''
        
        elif line.startswith('*'):
        
            line = re.sub(r'^\* +([^ ].*)+$', r'<li>\1</li>', line)
            
            if len(block_end) == 0:
                code.append('<ul>')
                block_end = '</ul>'
        
        else:
            if len(block_end) == 0:
                code.append('<p>')
                block_end = '</p>'
        
        line = re.sub(r'__([^_]+)__', r'<strong>\1</strong>', line)
       
        line = re.sub(r'_([^_]+)_', r'<em>\1</em>', line)

        code.append(line)

    if len(block_end) > 0:
        code.append(block_end)

    return ''.join(code)
