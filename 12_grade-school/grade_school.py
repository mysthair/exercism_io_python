class School(object):
    def __init__(self):
        self.students = {}

    def add_student(self, name, grade):
        self.students[ name ] = grade

    def roster(self):
        return [ s 
            for g in sorted(set(self.students.values()))
                for s in self.grade(g) ]

    def grade(self, grade_number):
        return sorted([ s 
            for s,g in self.students.items() 
                if g == grade_number ])

