def transform(legacy_data):
    return { letter.lower(): value
        for value, letters in legacy_data.items()
            for letter in letters
        }
    

