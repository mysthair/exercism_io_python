def is_isogram(texte : str) -> bool:

    return not [ c for c in texte.lower() if c.isalpha() and texte.lower().count(c) > 1]

