def prime_factors(natural_number):
    factors=[]
    p = 2
    while natural_number > 1:
        q, r = divmod(natural_number, p)
        while r == 0:
            factors.append(p)
            natural_number = q
            q, r = divmod(natural_number, p)
        p += 1
    return factors