from datetime import datetime, timedelta

def add_gigasecond(moment):
    assert(type(moment) == datetime)
    return moment + timedelta(seconds=10**9)
