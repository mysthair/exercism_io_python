def find_anagrams(word, candidates):

    return [ anagram for anagram in candidates 
        if sorted(word.lower()) == sorted(anagram.lower()) 
            and word.lower() != anagram.lower()]