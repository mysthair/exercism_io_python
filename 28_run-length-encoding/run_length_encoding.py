import re

def decode(string):

    return re.sub(r'(\d+)(\D)', lambda m: m[2] * int(m[1]), string)

def encode(string):

    return re.sub(r'(\D)\1+', lambda m: f'{len(m[0])}{m[1]}', string)