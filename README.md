# Python mini doc

## str

```python
type("Ceci est un texte.")
# <class 'str'>

#change case
"Ceci est un texte.".lower()
#'ceci est un texte.'
"Ceci est un texte.".upper()
#'CECI EST UN TEXTE.'
"une phrase. deux phrases.".capitalize()
#'Une phrase. deux phrases.'

#ramdom string
from secrets import choice
name = choice(ascii_uppercase) + choice(ascii_uppercase) + choice(digits) + choice(digits) + choice(digits)

#string from list / concat list of string with separator
''.join(list("lol mdr"))
"<SEPARATOR>".join(["ceci", " est ", " un", "texte", "lol"])
'ceci<SEPARATOR> est <SEPARATOR> un<SEPARATOR>texte<SEPARATOR>lol'

#cut text at \n
lines = text.splitlines() # text.splitlines(sep)
```

## formated string

```python
nom, age, debug = "Toto", 12, [1,2,3]
print("nom:%s, age:%i, debug:%r" % (nom, age, debug))       # nom:Toto, age:12, debug:[1, 2, 3]
print(f"nom:{nom}, age:{age}, debug:{debug}")               # nom:Toto, age:12, debug:[1, 2, 3]
print("nom:{}, age:{}, debug:{}".format(nom,age,debug))     # nom:Toto, age:12, debug:[1, 2, 3]
print("nom:{0}, age:{1}, debug:{2}".format(nom, age,debug)) # nom:Toto, age:12, debug:[1, 2, 3]
print("nom:{2}, age:{0}, debug:{1}".format(nom, age,debug)) # nom:[1, 2, 3], age:Toto, debug:12

# with size
hour, minute = 7, 8
"%02i:%02i" % (hour, minute) # '07:08'
f"{hour:02}:{minute:02}"     # '07:08'
"{0:02}:{1:02}".format(hour,minute) # '07:08'
#"{1:02}:{0:02}".format(minute,hour) # '07:08'

#aligment à gauche ou à droite
"###{:4}###{:<4}##{:^4}##{:>4}###".format("A","B","C","D")  # '###A   ###B   ## C  ##   D###'
"###{:4}###{:<4}##{:^4}##{:>4}###".format(1, 2, 3, 4)       # '###   1###2   ## 3  ##   4###'
```

## list, tuple

```python
#create new list
l1, l2, l3, l4 = [], list(), list("hello"), [ '12', '43', '-4']
# create list from str
list("un texte")
#['u', 'n', ' ', 't', 'e', 'x', 't', 'e']

#iterate on list
l5=[int(i) for i int l4]
#sum, len, max, min ...
s, l, ma, mi = sum(l5), len(l5), max(l5), min(l5) # ...

#sort list
l6=sorted(l5)
l5.sort(reverse=True)

#list copy or extracted copy
l2 = l1[:] # copy (not recursive copy)
# exemple
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][4:] # [4, 5, 6, 7, 8, 9]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][:8] # [0, 1, 2, 3, 4, 5, 7]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][4:8] # [4, 5, 6, 7]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][2:8:2] # [2, 4, 6]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][2:8:3] # [2, 5]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][-2:] # [8, 9]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][:-2] # [0, 1, 2, 3, 4, 5, 7]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][-4:-2] # [6, 7]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][-6:-2:2] # [4, 6]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][6:1:-1] # [6, 5, 4, 3, 2]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9][6:1:-2] # [6, 4, 2]

#attention la copie n'est pas récursive (ie un atome est cloné mais pas les objet)
x = [ 1, [2, 3], 'a', 10.6, [4, 5]]
y = x[:]
y[2] = 'b'
y[1][0] = 57
y[4] = [6, 7]
y # [1, [57, 3], 'b', 10.6, [6, 7]]
x # [1, [57, 3], 'a', 10.6, [4, 5]]

# utilisation de zip
list(zip(['x','y','3'], ('a','b','c'), [12,13,14]))
# [('x', 'a', 12), ('y', 'b', 13), ('3', 'c', 14)]  # hé oui c'est une liste de tuple
list(zip(['x','y','3'], ('a','b','c','c','d'), [12,13,14,15]))  #c'est la longueur la plus courte qui compte (3)
# [('x', 'a', 12), ('y', 'b', 13), ('3', 'c', 14)]
tuple(zip(['x','y','3'], ('a','b','c','c','d'), [12,13,14,15]))
# (('x', 'a', 12), ('y', 'b', 13), ('3', 'c', 14))
#matrix transpostion
list(zip(*[[1,2,3],[4,5,6],[7,8,9]])) # ça peut toujours servir
# [(1, 4, 7), (2, 5, 8), (3, 6, 9)]
```

## Set

```python
# create set from sring
set("un texte") # {' ', 'x', 'u', 'n', 't', 'e'}

# le nombre de caractère d'une phrase
len(set(string.ascii_lowercase).intersection(sentence.lower()))

# intersection, union, différence entre 2 ensembles
set("toto1234").intersection("3456") # {'4', '3'}
set("toto1234").union("3456") # {'5', '2', '4', '6', '3', 'o', '1', 't'}
# à gauche mais pas à droite
set("toto1234").difference("3456") # {'2', 'o', '1', 't'}

# utilisation de zip
set(zip(['x','y','3','3'], ('a','b','c','c','d'), [12,13,14,14,15]))
# {('y', 'b', 13), ('x', 'a', 12), ('3', 'c', 14)}
```

## Dict

```python
d = {}
d = { 'A': 12, 'B': 'haha' }
d = dict((('A', 1), (2,'B'), ('C', [10,11,12]))) # {'A': 1, 2: 'B', 'C': [10, 11, 12]}
d = dict([('A', 1), (2,'B'), ('C', [10,11,12])]) # {'A': 1, 2: 'B', 'C': [10, 11, 12]}
# all keys with same value
d = dict.fromkeys(['A', 'B', 'C'], 'toto' )  # {'A': 'toto', 'B': 'toto', 'C': 'toto'}

# update/add dict from other dicts
d.update(**dict1, **dict2)

# create from other dicts
d = { **dict1, **dict2, **dict3 }
# rq : **dict is a tuple mais il est implicitement convertie en dict  

# all the keys:
d.keys()
#all the values:
d.values()

#iterate the dict
[ f(k,v) for k,v in d.items() ]

#test if key exist
if key in d:  #d.keys() is ok too
    print(f"key '{key}' is in {d}.")
if key not in d:
    print(f"key '{key}' is not in {d}.")

#default value for not existing key
d.get(key, -1)   #return -1 if key not in d

#test existing value
if v in d.values():
    print(f"value '{v}' is in {d}")

dict(zip(('a', 'b', 'c', 'c', 'd'), [12, 13, 14, 15, 16]))
#{'a': 12, 'b': 13, 'c': 15}

#remove a key/value from dict
my_dict.pop('key')  # renvoie l'élément, ça peut toujours servir
if 'key' in myDict: del myDict['key']

#compter les occurences, renvois un dict
from collections import Counter
dict(Counter(  ('z', 'a', 'b', 'c', 'c', 'd') ))
# {'z': 1, 'a': 1, 'b': 1, 'c': 2, 'd': 1}
#Counter( yourIteratable )

```

## regexp

```python
#rechercher le motif a-z+
import re
txt="Ceci est un texte."
re.findall(r'[a-z]+', txt)
# ['eci', 'est', 'un', 'texte']

# substituer
re.sub(r'e([a-z]+)t', r'e\1-\1t', txt)

# chercher les mots encadrès ou non par un motif, sans le présenter dans le résultat (ici un guillemet simple, qui est accepté dans les mots recherchés )
re.findall(r"(?!')[a-z0-9']+(?<!')", phrase.lower())
```

## Function

```python
# optionnal parameter
def myfunc(name='toto'):
    pass

#howto specify parameter type
def myfunc(toto: int, bibi='lol': str) -> bool:
    return True
```

## Class

```python
class Myclass:
    staticParam=0
    #constructor
    def __init__(self, param1='titi'):
        self.param1 = param1
        pass
    def method1(self, param):
        pass
    @static
    def truc(param):
        Myclass.staticParam = param
myObject =  MyClass('foo')
myObject.method1('test')
Myclass.truc('titi')
```

## json

```python
import json
print("%r" % json.dumps({'name':'Titi', 'size':4.6, 'data':[0, 1,'a']}))
# '{"name": "Titi", "size": 4.6, "data": [0, 1, "a"]}'
print("%r" % json.loads('{"name": "Titi", "size": 4.6, "data": [0, 1, "a"]}'))
# {'name': 'Titi', 'size': 4.6, 'data': [0, 1, 'a']}

print("%r" % json.dumps({'name':'Titi', 'size':4.6, 'data':[0, 1,'a']}, indent=4, sort_keys=True))
# '{\n    "data": [\n        0,\n        1,\n        "a"\n    ],\n    "name": "Titi",\n    "size": 4.6\n}'
# ie: """{
#     "data": [
#         0,
#         1,
#         "a"
#     ],
#     "name": "Titi",
#     "size": 4.6
# }"""
```

## lambda function

A lambda function is a function without name

### function without name

```python
# pour passer une fonction en parametre sans avoir besoin de la nommer
a = myfunc(42, lambda x:2+len(set(x))
```

### sorted with key lambda
```python
#sort list with use function on element
#trier une liste en utilisant une fonction pour specifier la valeur de chaque élément
sorted(liste, key=lambda e:f(e))
#exemple: ici tab est un tableau de dict, que l'on veut trier suivant la clé 'P', et secondairement suivant la clé 'Team'... balaise hein ?
sorted(sorted(tab, key=lambda s:s['Team']), key=lambda r:r['P'], reverse=True)
```

### lambda in regexp
```python
# remplacer un nombre suivit d'une lettre par cette lettre répétée le nombre de fois (comment ça gnii ?)
re.sub(r'(\d+)(\D)', lambda m: m[2] * int(m[1]), string)
# une traduction est nécessaire?
re.sub(r'(\D)\1+', lambda m: f'{len(m[0])}{m[1]}', string)
```
