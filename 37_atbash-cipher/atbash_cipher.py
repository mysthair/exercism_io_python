from string import ascii_uppercase, ascii_lowercase, digits

ATBASH = dict(zip(ascii_lowercase, reversed(ascii_lowercase)))
ATBASH.update(dict(zip(ascii_uppercase, reversed(ascii_lowercase))))
ATBASH.update(dict(zip(digits,digits)))

import re


def encode(plain_text):
    
    return ' '.join(re.findall(r'.{1,5}', decode(plain_text))) 

def decode(ciphered_text):

    return ''.join(ATBASH.get(c, '') for c in ciphered_text)
