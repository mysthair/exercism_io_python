def largest_product(series, size):
    if size < 0:
        raise ValueError("invalid value")
    def prod(lst):
        return int(lst.pop()) * prod(lst) if len(lst) > 0 else 1
    return max( prod(list(series[i:i + size:])) for i in range(0, len(series) + 1 - size) )
