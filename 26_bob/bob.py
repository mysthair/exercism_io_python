def hey(phrase):
    phrase = phrase.strip()
    if len(phrase) == 0:
        return 'Fine. Be that way!'
    if phrase[-1] == '?':
        if 'YOU' in phrase.split():
            return "Calm down, I know what I'm doing!"
        return "Sure."
    elif 'You' in phrase.split():
        return 'Fine. Be that way!'
    elif phrase.isupper():
        return 'Whoa, chill out!'
    else:
        return 'Whatever.'
