def slices(series, length):
    if length <= 0 or length > len(series) or len(series) == 0:
        raise ValueError("Invalid Input Values")
    
    return [ series[i:i+length] for i in range(0, len(series) - length + 1)]

    
