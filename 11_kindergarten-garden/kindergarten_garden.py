class Garden(object):
    PLANTS = { v[0]:v  for v in [ 'Grass', 'Clover', 'Violets', 'Radishes' ] } 

    def __init__(self, diagram, students=['Alice', 'Bob', 'Charlie', 'David',
'Eve', 'Fred', 'Ginny', 'Harriet', 'Ileana', 'Joseph', 'Kincaid', 'Larry']):

        lines = diagram.splitlines() 

        self.students = sorted(students)
   
        self._plants = [[ Garden.PLANTS[lines[i][index * 2 + j ]]
            for i in range(0, 2)
                for j in range(0, 2) 
                    if index < len(lines[i]) // 2
                       ] for index in range(0, len(students)) ]

    def plants(self, student):
    
        return self._plants[ self.students.index(student) ]

