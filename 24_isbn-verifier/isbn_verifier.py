def verify(isbn):

    digits = [ int(c) for c in isbn if c.isdigit() ]

    if digits == []:
        return False
    elif isbn[-1] == 'X':
        digits.append(10)

    n = len(digits)
    return n == 10 and \
        0 == sum((n - i) * digits[i] for i in range(0,n)) % 11
