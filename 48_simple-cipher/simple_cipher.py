class Cipher(object):
    def __init__(self, key=None):
        if key == None:
            key = 'ldpdsdqgdehdu' # 'a' * 10  is ok too...
        self.key = key[:]

    def encode(self, text):
        return ''.join( chr(ord('a') + (ord(c) + ord(k) - 2 *  ord('a')) % 26) 
            for c, k in zip(text, self.key * (1 + len(text) // len(self.key))) )

    def decode(self, text):
        return ''.join( chr(ord('a') + (ord(c) - ord(k)) % 26) 
            for c, k in zip(text, self.key * (1 + len(text) // len(self.key))) )
