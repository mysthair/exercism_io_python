def rotate(text, key):
    def rol(c, k):
        if c.islower():
            return chr(ord('a') + ((ord(c) - ord('a') + k) % 26))
        if c.isupper():
            return chr(ord('A') + ((ord(c) - ord('A') + k) % 26))
        return c

    return ''.join( rol(c, key) for c in text)
