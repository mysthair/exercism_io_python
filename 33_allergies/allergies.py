ALLERGENS = [ 'eggs', 'peanuts', 'shellfish', 'strawberries', 
            'tomatoes', 'chocolate', 'pollen', 'cats' ]

class Allergies(object):

    def __init__(self, score):
        self.score = score

    def is_allergic_to(self, item):
        return item in self.lst

    @property
    def lst(self):
        return [ ALLERGENS[i] 
            for i in range(0, len(ALLERGENS)) 
                if self.score & 1 << i ]