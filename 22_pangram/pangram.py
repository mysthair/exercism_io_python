import string


def is_pangram(sentence):

    return 26 == len(set(string.ascii_lowercase).intersection(sentence.lower()))

